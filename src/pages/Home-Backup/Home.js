import React from "react";
import "./Home.scss";

const Home = () => {
  return (
    <div id="home-page">
      <div className="text">Home</div>
    </div>
  );
};
export default Home;
