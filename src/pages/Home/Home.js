import React, { useEffect, useState } from "react";
import { Box, H1Green, H2Green2, Container } from "./HomeStyles";
import { H1, A } from "../../styles/Typography";
import Colors from "../../styles/Colors";
import { todayService } from "../../services/stat-covid-19";
import { commaFormat, dateFormat } from "../../util/format";
import CovidCard from "../../components/CovidCard/CovidCard";
import { NavLink, Link, useHistory } from "react-router-dom";
import { Button } from "reactstrap";
import Swal from "sweetalert2";

const Home = () => {


 const openAlert = ({title, amount}) =>{
   
  Swal.fire(
    title,
    `${amount}`,
    'success'
  )
 }

  
  // {"Confirmed":2067,"Recovered":612,"Hospitalized":1435,"Deaths":20,"NewConfirmed":89,"NewRecovered":31,"NewHospitalized":57,"NewDeaths":1,"UpdateDate":"04\/04\/2020 09:48","Source":"https:\/\/covid19.th-stat.com\/","DevBy":"https:\/\/www.kidkarnmai.com\/","SeverBy":"https:\/\/smilehost.asia\/"}
  const [data, setData] = useState({
    Confirmed: null,
    Recovered: null,
    Hospitalized: null,
    Deaths: null,
    NewConfirmed: null,
    NewRecovered: null,
    NewHospitalized: null,
    NewDeaths: null,
    UpdateDate: "",
    Source: "",
    DevBy: "",
    SeverBy: "",
  });
  const [count, setCount] = useState(0);

  useEffect(() => {
    console.log(count);
    if (data.UpdateDate == "") {
      const getToday = async () => {
        try {
          const response = await todayService();
          // check status = 200
          if (response.status == 200) {
            setData(response.data);
            console.log(response.data);
          } else {
            alert("error");
          }
        } catch {
          alert("error");
        }
      };
      getToday();
    }
  }, [count]);
  //[] = componentDidMount only เพื่อไม่ให้มันไปเรียก useEffect  componentDidUpdate
  //data

  const history = useHistory();
  
  const openCaseSum = () => {
    history.push("/casesum", { test: "123" });
  };
  const openTodoList = () => {
    history.push("/todolist", { test: "123" });
  };
  const openTodoListRedux = () => {
    history.push("/todolistredux", { test: "123" });
  }
  const openProfile = () => {
    history.push("/profile", { test: "123" });
  }
  const openTimeline = () => {
    history.push("/timeline", { test: "123" });
  }

  return (
    <>
      <Container>
        <Box>
          <H1 color={Colors.green} style={{ textAlign: "center" }}>
            รายงานสถานการณ์
          </H1>
          <H2Green2>อัพเดทข้อมูลล่าสุด : {data.UpdateDate}</H2Green2>
          <h3 style={{ textAlign: "center" }}>
            แถลงการณ์ประจำวันที่ {dateFormat(data.UpdateDate)}
          </h3>
          <p style={{ textAlign: "center" }}>
            ยืนยันผู้ติดเชื้อเพิ่ม {commaFormat(data.NewConfirmed)} ราย
            มีผู้เสียชีวิตเพิ่ม {commaFormat(data.NewDeaths)} ราย
            ทำให้ยอดสะสมอยู่ที่ {commaFormat(data.Confirmed)} ราย กลับบ้านแล้ว{" "}
            {commaFormat(data.Recovered)} ราย
          </p>
          <br />
          <div>
            <A
              color={Colors.primary}
              href="https://covid19.th-stat.com/th/self_screening"
            >
              แบบทดสอบประเมินความเสี่ยง
            </A>
          </div>
        </Box>
        <Box>
          {/* <div
          style={{
            margin: "10px auto",
            width: "95%",
            backgroundColor: "pink",
            color: "#fff",
            height: "150px",
            marginTop: "10px",
            marginBottom: "10px",
          }}
        ></div> */}
          {/* NewConfirmed: 33
​
NewDeaths: 3
​
NewHospitalized: -53
​
NewRecovered: 83 */}
          <div>
            <div style={{ display: "flex", flexDirection: "row" }}>
              <CovidCard
                color={Colors.primary}
                title="ติดเชื้อสะสม"
                amount={data.Confirmed}
                increase={data.NewConfirmed}
                onAlert={openAlert}
              ></CovidCard>
            </div>
            <div style={{ display: "flex", flexDirection: "row" }}>
              <CovidCard
                title="หายแล้ว"
                amount={data.Recovered}
                increase={data.NewRecovered}
                onAlert={openAlert}
              ></CovidCard>
              <CovidCard
                title="รักษาอยู่ใน รพ."
                color={Colors.green3}
                amount={data.Hospitalized}
                increase={data.NewHospitalized}
                onAlert={openAlert}
              ></CovidCard>
              <CovidCard
                title="เสียชีวิต"
                color={Colors.gray}
                amount={data.Deaths}
                increase={data.NewDeaths}
                onAlert={openAlert}
              ></CovidCard>
            </div>
          </div>

          <NavLink
            to={{
              pathname: "/casesum",
              state: { param: "test" },
            }}
          >
            CaseSum
          </NavLink>
          <Link to="/about">About</Link>
          <Button onClick={openCaseSum}> Click CaseSum</Button>
          <Button onClick={openTodoList}> Click TodoList</Button>
          <br/>
          <Button onClick={openTodoListRedux}> Click TodoList Redux</Button>
          <br/>
          <Button onClick={openTimeline}> Click Timeline</Button>
          <br/>
          <Button onClick={openProfile}> Click Profile</Button>



          {/* <DivBox><h2>xxx</h2></DivBox> */}
          {/* <DivBox></DivBox>
        <DivBox marginRight={0}></DivBox> */}
          {/* <div style={{display: "inline-block", margin: "10px 19px 10px auto", width: "30%", backgroundColor: "pink", color: "#fff", height: "150px"}}></div>
        <div style={{display: "inline-block", margin: "10px 19px 10px auto", width: "30%", backgroundColor: "pink", color: "#fff", height: "150px"}}></div>
        <div style={{display: "inline-block", margin: "10px auto", width: "30%", backgroundColor: "pink", color: "#fff", height: "150px"}}></div> */}
          {/* <div style={{display: "inline-block",width: "30%", backgroundColor: "pink", color: "#fff", height: "150px", margin: "5px"}}></div>
        <div style={{display: "inline-block",width: "30%", backgroundColor: "pink", color: "#fff", height: "150px"}}></div> */}
        </Box>
      </Container>

      {/* <button onClick={() => setCount(count + 1)}>count</button> */}
    </>
  );
};
export default Home;
