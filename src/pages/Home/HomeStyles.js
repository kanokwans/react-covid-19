import Styled from "styled-components";
import { H1, H2 } from "../../styles/Typography";
import Colors from "../../styles/Colors";

export const Container = Styled.div`
    display: flex;
    flex-direction: row;
`;

export const Box = Styled.div`
    display: flex;
    flex: 1;
    flex-direction: column;
    text-align: center;
    padding: 10px;
`;

export const H1Green = Styled(H1)`
    color: ${Colors.green};
`;

export const H2Green2 = Styled(H2)`
    color: ${Colors.green2};
`;
//  <div style={{display: "inline-block", margin: "10px 19px 10px auto", width: "30%", backgroundColor: "pink", color: "#fff", height: "150px"}}></div>

