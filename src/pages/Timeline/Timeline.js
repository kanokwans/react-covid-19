import React, { useEffect, useState } from "react";
import { timelineService } from "../../services/stat-covid-19";
import { Table, Container } from "reactstrap";
import { dateFormMMDDYYYY, commaFormat } from "../../util/format";
import SmartTable from "../../components/Table/Table";
const Timeline = () => {
    
    const [timelineList, setTimelineList] = useState([]);
    useEffect(() => {

        const timeline = async () => {

            const { status, data } = await timelineService();        
            if(status === 200) {
                console.log('data : ',data);
                setTimelineList(data.Data);
            }
        }
        timeline();
    }, []);

   /**
     * Date	"01/01/2020"
NewConfirmed	0
NewRecovered	0
NewHospitalized	0
NewDeaths	0
Confirmed	0
Recovered	0
Hospitalized	0
Deaths	0
     */

     const genRow = () => {
        let html = [];
        timelineList.map(({Date, NewConfirmed, NewRecovered, NewHospitalized, NewDeaths, Confirmed, Recovered, Hospitalized, Deaths}, 
            key) => {
            // console.log('key : ', key);
            // console.log('item :  Date, NewConfirmed, NewRecovered, NewHospitalized, NewDeaths, Confirmed, Recovered, Hospitalized, Deaths ', Date, NewConfirmed, NewRecovered, NewHospitalized, NewDeaths, Confirmed, Recovered, Hospitalized, Deaths);
            html = [...html, 
                <tr>
                    <td>{key + 1}</td>
                    <td>{dateFormMMDDYYYY(Date)}</td>
                    <td>{commaFormat(NewConfirmed)}</td>
                    <td>{commaFormat(NewRecovered)}</td>
                    <td>{commaFormat(NewHospitalized)}</td>
                    <td>{commaFormat(NewDeaths)}</td>
                    <td>{commaFormat(Confirmed)}</td>
                    <td>{commaFormat(Recovered)}</td>
                    <td>{commaFormat(Hospitalized)}</td>
                    <td>{commaFormat(Deaths)}</td>   
                </tr>];
        })
        return html;
     }
    return <>
    <Container>
    <SmartTable>
        <thead className="bg-info text-center">
            <tr>
                <th>#</th>
                <th>Date</th>
                <th>NewConfirmed</th>
                <th>NewRecovered</th>
                <th>NewHospitalized</th>
                <th>NewDeaths</th>
                <th>Confirmed</th>
                <th>Recovered</th>
                <th>Hospitalized</th>
                <th>Deaths</th>
            </tr>
        </thead>
        <tbody className="text-center">
            { genRow()}
            
        </tbody>
    </SmartTable>
    </Container></>
}

export default Timeline;


