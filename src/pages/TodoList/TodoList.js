import React, { useState} from "react";
import { NavLink, Link, useHistory } from "react-router-dom";

import {
  Card,
  Layout,
  Menu,
  Breadcrumb,
  Row,
  Col,
  Form,
  Input,
  InputNumber,
  Button,
} from "antd";
import {
  UserOutlined,
  LaptopOutlined,
  NotificationOutlined,
} from "@ant-design/icons";
import CardForm from "../../components/CardForm/CardForm";
import CardList from "../../components/CardList/CardList";

const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;
const style = { padding: "20px" };
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const TodoList = () => {


  const [todo, setTodo] = useState();

  const onFinish = (values) => {
    console.log(values);
  };

  const validateMessages = {
    required: "${label} is required!",
  };

  const history = useHistory();
  const openHome = () => {
    history.push("/", { test: "123" });
  };
  return (
    <>
          <Layout style={{ padding: "0 24px 24px" }}>
            <Breadcrumb style={{ margin: "16px 0" }}>
              <Breadcrumb.Item>Home</Breadcrumb.Item>
              <Breadcrumb.Item onClick={openHome}>TodoList</Breadcrumb.Item>
            </Breadcrumb>
            <Content
              className="site-layout-background"
              style={{
                padding: 24,
                margin: 0,
                minHeight: 280,
              }}
            >
              <Row>
                <Col span={12}>
                <CardForm onTodo={setTodo}></CardForm>
                </Col>
                <Col span={12}>
                 <CardList todo={todo}></CardList>
                </Col>
              </Row>
            </Content>
          </Layout>
    </>
  );
};

export default TodoList;
