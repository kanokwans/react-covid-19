import React, { useState, useEffect } from "react";
import { Title } from "../../styles/Typography";
import Styled from "styled-components";
import {
  Table,
  Button,
  Container,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";
import { useLocation, useParams } from "react-router-dom";
import { caseSumService } from "../../services/stat-covid-19";
import { commaFormat } from "../../util/format";

const TitleExtend = Styled(Title)`
    text-decoration: underline;
`;

const CaseSum = () => {


  const location = useLocation();
  const params = useParams();
  const [provinces, setProvinces] = useState([]);
  // กำหนดตัวแปรให้น้อยที่สุด

  const [currentPage, setCurrentPage] = useState(1);
  const [totalPage, setTotalPage] = useState(0);
  const [totalItem, setTotalItem] = useState(0);
  const [dropdownOpen, setDropdownOpen] = useState(false);
  // let totalItem = 0;
  // let totalPage = 0;
  let [itemPerPage, setItemPerPage] = useState(10);

  useEffect(() => {
    const caseSum = async () => {
      const { data, status } = await caseSumService();

      if (status == 200) {
        // console.info(data);
        let objectToArray = [];
        for (let key in data.Province) {
          objectToArray = [
            ...objectToArray,
            { province: key, amount: data.Province[key] },
          ];
        }

        // console.log("objectToArray ; ", objectToArray);
        setProvinces(objectToArray);
        // เอาค่าจาก state ไปใช้ทันที ไม่ได้เลยต้องประกาศตัวแปรเก็บค่าก่อนค่อยนำไปใช้ต่อได้
        const total = Object.keys(data.Province).length;
        setTotalItem(total);

        setTotalPage(Math.ceil(total / itemPerPage));
      }
    };
    caseSum();
  }, []);

  const onPreviousPage = () => {
    setCurrentPage(currentPage - 1);
  };

  const onNextPage = () => {
    setCurrentPage(currentPage + 1);
  };

  const generateRow = () => {
    let html = []; // multi chlidren
    let html2 = [];

    // perpage 10
    // data = 77

    // current = 5 previous = 4, next = 6 , ---> range  41 - 50
    // current , previous , next

    // สูตร Max =  current x perpage = 50
    // สูตร Min =  (current x perpage) - perpage + 1  = 41

    // สูตร totalPage =  77/10 ปัดขึ้น = 8
    // สูตร previous = current - 1 = 4 มีผลต่อการ disable, enable --> current  = 1 disabled
    // สูตร next = current + 1 = 6 มีผลต่อการ disable, enable  --> current = totalPage disabled

    // for (let key in provinces) {
    //   // console.log(key, provinces[key]);
    //   html = [
    //     ...html,
    //     <tr>
    //       <th>{key}</th>
    //       <td>{commaFormat(provinces[key])}</td>
    //     </tr>,
    //   ];
    // }
    // provinces.map((item, key) => {
    //      html = [
    //     ...html,
    //     <tr>
    //       <th>{item.province}</th>
    //       <td>{commaFormat(item.amount)}</td>
    //     </tr>,
    //   ];
    // })

    //     provinces.map(({province, amount}, key) => {

    //       html = [
    //      ...html,
    //      <tr>
    //        <th>{province}</th>
    //        <td>{commaFormat(amount)}</td>
    //      </tr>,
    //    ];
    //  })

    // สูตร Max =  current x perpage = 50
    // สูตร Min =  (current x perpage) - perpage + 1  = 41
    if (provinces.length > 0) {
      let min = currentPage * itemPerPage - itemPerPage;
      let max = currentPage == totalPage ? totalItem : currentPage * itemPerPage;
      // console.log("min : ", min);
      // console.log("max : ", max);

      for (let i = min; i < max; i++) {
        const { province, amount } = provinces[i];
        // console.log(province);
        html = [
          ...html,
          <tr key={i} className={ amount > 200 ? "bg-danger" : amount > 100  ? "bg-warning":  amount > 50 ? "bg-success": "" }>
            <td>{i + 1}</td>
            <td>{province}</td>
            <td>{ amount > 50 ? <b>{commaFormat(amount)}</b> : <span>{commaFormat(amount)}</span>}</td>
          </tr>,
        ];
      }
    }

    return html;
  };

  console.log(location);
  console.log(params);
  // return <TitleExtend>
  //         About
  //         <p>paragraph</p>
  //       </TitleExtend>
  // console.log("rerender");

  const calTotalPage = (item) => {

    
    // setTotalPage(Math.ceil(totalPage / item));
    // setItemPerPage(item);

  }
  return (
    <>
      <Container>
        <Dropdown
          group
          isOpen={dropdownOpen}
          size="sm"
          toggle={() => setDropdownOpen(!dropdownOpen)}
        >
          <DropdownToggle caret>{itemPerPage}</DropdownToggle>
          <DropdownMenu>
            <DropdownItem onClick={() => {calTotalPage(5)}}>5</DropdownItem>
            <DropdownItem onClick={() => {calTotalPage(10)}}>10</DropdownItem>
            <DropdownItem onClick={() => {calTotalPage(20)}}>20</DropdownItem>
          </DropdownMenu>
        </Dropdown>
        <Table striped bordered>
          <thead className="bg-primary">
            <tr>
              <th>#</th>
              <th>Provice</th>
              <th>Amount</th>
            </tr>
          </thead>
          <tbody>
            {generateRow()}
            {/* <tr>
            <th scope="row">1</th>
            <td>Mark</td>
          </tr> */}
          </tbody>
        </Table>
        <Button
          disabled={currentPage == 1}
          color="info"
          onClick={onPreviousPage}
        >
          previous
        </Button>
        currentPage : {currentPage} totalPage : {totalPage} totalItem :{" "}
        {totalItem}
        <Button
          disabled={currentPage == totalPage}
          color="info"
          onClick={onNextPage}

          className="float-right"
        >
          next
        </Button>
      </Container>

      <Button color="danger">Danger!</Button>
    </>
  );
};
// class CaseSum extends Component {

//   componentDidMount() {
//     console.log("componentDidMount");
//   }

//   render() {
//     return (
//       <TitleExtend>
//         About
//         <p>paragraph</p>
//       </TitleExtend>
//     );
//   }
// }
export default CaseSum;
