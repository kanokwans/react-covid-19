import React, { useEffect, useState } from "react";
import { profileService } from "../../services/stat-covid-19";
import { Container, CardImg, Card } from "reactstrap";
import SmartTable from "../../components/Table/Table";

const Profile = () => {

    const [profileList, setProfileList] = useState([]);

    useEffect(() => {

        const profile = async () => {

            const { status, data } = await profileService();
            if (status === 200) {
                console.log('data : ', data.results);
                setProfileList(data.results);
            }
        }
        profile();
    }, []);

    /**
     * 
     {"results":
          [
              {"gender":"female",
               "name": {"title":"Miss","first":"بهار","last":"موسوی"},
               "location":
                         {"street":
                             {"number":8426,"name":"خاوران"},
                                 "city":"گلستان","state":"خوزستان","country":"Iran",
                                 "postcode":16381,"coordinates": {"latitude":"29.7064","longitude":"91.1388"},
                                 "timezone":{"offset":"+8:00","description":"Beijing, Perth, Singapore, Hong Kong"}
                         },
               "email":"bhr.mwswy@example.com",
               "login":{"uuid":"3a3dd3f6-125e-409c-92e3-e645fc38d884","username":"browntiger487","password":"terrell","salt":"kw5cLXxl","md5":"96291ec6c6f19503a7048dfde56d0b4f","sha1":"efa9b9057d00ac700dcfee4d6da314c228aec3ff","sha256":"624fc2dd83ba1413c6638762af578ee72a2569131b7104e0e83e8d5a6e7d12ba"},"dob":{"date":"1958-04-14T02:03:05.298Z","age":62},
               "registered":{"date":"2008-07-06T05:32:12.590Z","age":12},
               "phone":"072-58553718","cell":"0918-681-1604",
               "id":{"name":"","value":null},
               "picture":{"large":"https://randomuser.me/api/portraits/women/96.jpg","medium":"https://randomuser.me/api/portraits/med/women/96.jpg","thumbnail":"https://randomuser.me/api/portraits/thumb/women/96.jpg"},"nat":"IR"}],"info":{"seed":"7aebe9c6446dcef0","results":1,"page":1,"version":"1.3"}}
 
     */


    const genRow = () => {
        let html = [];
        profileList.map(({ name: { title, first, last }, gender, location: { street: { number } }, email, phone, picture: { thumbnail } },
            key) => {
            console.log('first : ', first);
            html = [...html,
            <tr>
                <td>{ key + 1 }</td>
                <td>{ title+' '+first+' '+last }</td>
                <td>{ gender }</td>
                <td>{ number }</td>
                <td>{ email }</td>
                <td>{ phone }</td>
                <td>
                    <Card>
                        <CardImg top width="100%" src={thumbnail} alt="Card image cap" />
                    </Card>
                </td>
            </tr>];
        })
        return html;
    }

    console.log('xxxx');
    return <><Container>
        <SmartTable>
            <thead className="bg-info text-center">
                <tr>
                    <th>#</th>
                    <th>name</th>
                    <th>gender</th>
                    <th>location</th>
                    <th>email</th>
                    <th>phone</th>
                    <th>picture</th>
                </tr>
            </thead>
            <tbody className="text-center">
                {genRow()}
            </tbody>
        </SmartTable>
    </Container></>;
}
export default Profile;