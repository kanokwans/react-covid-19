import axios from "axios";

export const todayService = () => {
    return axios.get("https://covid19.th-stat.com/api/open/today");
}

export const caseSumService = () => {
    return axios.get("https://covid19.th-stat.com/api/open/cases/sum");
}
export const timelineService = () => {
    return axios.get("https://covid19.th-stat.com/api/open/timeline");
}
export const profileService = (results = 100) => {
    return axios.get(`https://randomuser.me/api/?results=${results}`);
}
