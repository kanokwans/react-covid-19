import { PUSH_TODOLIST, CLEAR_TODOLIST } from "../constants";

//payload = data ที่ส่งเข้ามานะ (ถ้ามี)
export const pushTodoList = payload => ({
    type: PUSH_TODOLIST,
    payload: payload
})
// return () แบบสั่น 
export const clearTodoList = () => ({
    type: CLEAR_TODOLIST,
    payload: {}
})