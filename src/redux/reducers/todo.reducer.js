import  { PUSH_TODOLIST, CLEAR_TODOLIST } from "../constants";

const initialState = {
    isLoading: false,
    data: {}
};

//state ตั้งต้น กับ actions
export default (state = initialState, { type, payload }) => {
    switch (type) {
        case PUSH_TODOLIST:
            return { ...state, isLoading: true, data: payload };
        case CLEAR_TODOLIST:
            return { ...state , isLoading: false };
        
        default:
            return state;

    }
}