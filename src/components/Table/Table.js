import React, { useState, useEffect } from "react";
import { Table, Container, Pagination, PaginationItem, PaginationLink } from "reactstrap";


let currentPage = 1;
const SmartTable = ({children}) => {
     console.log(children);
     const [totalPage, setTotalPage] = useState(0);
     const [tbody, setTbody] = useState([]);
    // const [totalItem, setTotalItem] = useState(0);
    const itemPerPage = 10;


    // let totalItem = 0;
    const totalItem = children[1].props.children ? children[1].props.children.length: 0;
    useEffect(() => {
        
        console.log('useEffect');
        // console.log(children[1].props.children.length);
        if(totalItem > 0) {
            setTotalPage(Math.ceil(totalItem / itemPerPage));
            genTbody(currentPage);
        }
        
        
    }, [children])
 

    console.log('totalItem : ', totalItem);
    

    const changePage = (page) => {

        console.log("page : ", page);
        // setCurrentPage(page); // ไม่สามารถใช้ค่าใหม่ได้ทันที ที่มีการ setSate 
        currentPage = page;
        genTbody(page);
    }

    const nextPage = () =>{
        if(currentPage < totalPage){
            currentPage = currentPage + 1;
            genTbody(currentPage);
        }
    }
    const previousPage = () => {
        
        if(currentPage != 1){
            currentPage = currentPage - 1;
            genTbody(currentPage);
        }
    }

    const genPaging = () =>{

        let html = [<PaginationItem>
            <PaginationLink previous href="#" onClick={()=>{
                    previousPage();
                }}></PaginationLink>
        </PaginationItem>];
        
         for (let i = 0; i < totalPage; i++) {
             html = [...html, 
             <PaginationItem>
                <PaginationLink href="#" onClick={()=>{
                    changePage(i+1);
                }}>
                {/* <PaginationLink href="#"> */}
                 {i+1}
                </PaginationLink>
             </PaginationItem>]

        }
        html = [...html, 
            <PaginationItem>
               <PaginationLink next href="#" onClick={()=>{
                   nextPage();
               }}>
               {/* <PaginationLink href="#"> */}
               </PaginationLink>
            </PaginationItem>]
        return html;
    }
    const genTbody = (page) =>{

        let html = [];
        
        let min = (page * itemPerPage) - itemPerPage;
        let max = page == totalPage ? totalItem : page * itemPerPage;
        
        console.log('min : ', min);
        console.log('max : ', max);

        if(typeof children[1] != 'undefined') {
            for (let i = min; i < max; i++) {
                html = [...html, children[1].props.children[i]];
            }
            // children[1].props.children;
            // return html;
            setTbody(html);

        }
        
    }

    return <><Table striped hover>
        {/* <thead>
            <tr>
                <th>Date</th>
                <th>NewConfirmed</th>
                <th>NewRecovered</th>
                <th>NewHospitalized</th>
    <th>NewDeaths</th>
                <th>Confirmed</th>
                <th>Recovered</th>
                <th>Hospitalized</th>
                <th>Deaths</th>
            </tr>
        </thead>
        <tbody>

        </tbody> */}
        {children[0]}

        <tbody>
        {/* {children[1].props.children} */}
        { 
            tbody
        }
        </tbody>
        {/* {children[1]} */}
    </Table>
    
    <Pagination aria-label="Page navigation example">
            {/* <PaginationItem>
                <PaginationLink first href="#" />
            </PaginationItem>
            <PaginationItem>
                <PaginationLink previous href="#" />
            </PaginationItem> */}
           { genPaging()}
            {/* <PaginationItem>
                <PaginationLink href="#">
                    1
        </PaginationLink>
            </PaginationItem>
            <PaginationItem>
                <PaginationLink href="#">
                    2
        </PaginationLink>
            </PaginationItem>
            <PaginationItem>
                <PaginationLink href="#">
                    3
        </PaginationLink>
            </PaginationItem>
            <PaginationItem>
                <PaginationLink href="#">
                    4
        </PaginationLink>
            </PaginationItem>
            <PaginationItem>
                <PaginationLink href="#">
                    5
        </PaginationLink>
            </PaginationItem> */}
            {/* <PaginationItem>
                <PaginationLink next href="#" />
            </PaginationItem>
            <PaginationItem>
                <PaginationLink last href="#" />
            </PaginationItem>  */}
        </Pagination>
        totalItem: {totalItem}
    totalPage: {totalPage}
    currentPage: { currentPage}</>
}
export default SmartTable;