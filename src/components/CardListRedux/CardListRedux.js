import React, { useState, useEffect } from "react";
import { Card, List } from "antd";
import { useSelector } from "react-redux";
let listTodo = [];
const CardListRedux = () => {

  const style = { padding: "20px" };
  // const [listTodo, setListTodo] = useState([]);

  

  // map state เข้า props useSelelector = state 
 // ทำให้ไม่สามารถ set ค่าเข้า state ได้ เลยต้อง set เข้า ตัวแปรธรรมดา
  const { isLoading, data } = useSelector(state => state.todoReducer); // ถูกมัดไว้ใน index และการสร้างควรเป็นเรื่องเดียวกันใน file เดียวกัน
  if (isLoading) {
    listTodo = [...listTodo , data];
  }

  return (
    <div>
      <div style={style}>
        <Card title="To Do List" bordered={true}>
          {/* title : {todo.title} */}
          <List
            itemLayout="horizontal"
            dataSource={listTodo}
            renderItem={({ title, description }) => (
              <List.Item>
                <List.Item.Meta
                  title={<a href="https://ant.design">{title}</a>}
                  description={description}
                />
              </List.Item>
            )}
          />
        </Card>
      </div>
    </div>
  );
};

export default CardListRedux;
