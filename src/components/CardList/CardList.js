import React, { useState, useEffect } from "react";
import { Card, List } from "antd";
const CardList = ({ todo }) => {
  const style = { padding: "20px" };
  const [listTodo, setListTodo] = useState([]);

  useEffect(() => {
    if (todo) {
      const list = [...listTodo, todo];

      setListTodo(list);
      console.log("listTodo : ", list);
    }
  }, [todo]);

  return (
    <div>
      <div style={style}>
        <Card title="To Do List" bordered={true}>
          {/* title : {todo.title} */}
          <List
            itemLayout="horizontal"
            dataSource={listTodo}
            renderItem={({ title, description }) => (
              <List.Item>
                <List.Item.Meta
                  title={<a href="https://ant.design">{title}</a>}
                  description={description}
                />
              </List.Item>
            )}
          />
        </Card>
      </div>
    </div>
  );
};

export default CardList;
