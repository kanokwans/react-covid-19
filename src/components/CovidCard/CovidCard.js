import React, { useEffect} from "react";
import { Box, Title, Amount, Increase } from "./CovidCardStyle";
import { commaFormat } from "../../util/format";


const CovidCard = ({color, title, amount = 0, increase = 0, onAlert}) => {

 let textIncrease = "";
 if(increase > -1){
    textIncrease = "เพิ่มขึ้น";
 }else {
    textIncrease = "ลดลง";

 }
 const openAlert = () => {
   onAlert({title, amount});

 }
  
  return (
    <>
      <Box color={color} onClick={openAlert}  >
        <Title>{title}</Title>
        <Amount>{commaFormat(amount)}</Amount>
        <Increase>{textIncrease} {commaFormat(Math.abs(increase))}</Increase>
      </Box>
    </>
  );
};
export default CovidCard;
