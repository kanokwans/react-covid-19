import Styled from "styled-components";
import Colors from "../../styles/Colors";

// export const DivBox = Styled.div`
//     width: 30%;
//     width: ${({ width }) => width ? width +";" : "30%;" }
//     background-color: ${Colors.green2};
//     display: inline-block;
//     ${({ marginRight }) => (marginRight >= 0 ? " margin: 10px " + marginRight + "px 10px auto;" : " margin: 10px 19px 10px auto;")}
//     color: #000;
//     height: 150px;
// `;

export const Box = Styled.div`
    display: flex;
    flex: 1;
    flex-direction: column;
    background-color: ${Colors.green2};
    margin: 5px;
    padding: 10px;
    background-color:  ${({ color }) => color ? color : Colors.green2 }
`;
const textStyle = `
    text-align: center;
    color: white;
    margin: 0px;
`;
export const Title = Styled.h3`
    ${textStyle}
`;
export const Amount = Styled.h1`
    ${textStyle}
`;
export const Increase = Styled.h4`
    ${textStyle}
`;

