import React from "react";
import {
    Card,
    Layout,
    Breadcrumb,
    Row,
    Col,
    Form,
    Input,
    Button,
  } from "antd";
const CardForm = ({ onTodo }) => {
    const layout = {
        labelCol: {
          span: 8,
        },
        wrapperCol: {
          span: 16,
        },
      };
    const style = { padding: "20px" };
    const onFinish = ({ todo }) => {
        // console.log(values);
        onTodo(todo);
        form.resetFields();
        
      };
    
    const validateMessages = {
    required: "${label} is required!",
    };

    const [form] = Form.useForm();

 return <>
 <div style={style}>
                <Card title="Card title" bordered={true}>
                  <Form form={form}
                    {...layout}
                    name="nest-messages"
                    onFinish={onFinish}
                    validateMessages={validateMessages}
                  >
                    <Form.Item
                      name={["todo", "title"]}
                      label="Title"
                      rules={[
                        {
                          required: true,
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>
                    <Form.Item
                      name={["todo", "description"]}
                      label="To Do"
                      rules={[
                        {
                          required: true,
                        },
                      ]}
                    >
                      <Input.TextArea />
                    </Form.Item>
                    <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                      <Button type="primary" htmlType="submit">
                        Submit
                      </Button>
                    </Form.Item>
                  </Form>
                </Card>
              </div>
 </>   
}

export default CardForm;