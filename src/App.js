import React, { Suspense } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
// import Home from "./pages/Home/Home";
// import About from "./pages/About/About";
import "./App.css";

const Home = React.lazy(() => import("./pages/Home/Home"));
const CaseSum = React.lazy(() => import("./pages/CaseSum/CaseSum"));
const TodoList = React.lazy(() => import("./pages/TodoList/TodoList"));
const TodoListRedux = React.lazy(() => import("./pages/TodoListRedux/TodoListRedux"));
const Timeline = React.lazy(() => import("./pages/Timeline/Timeline"));
const Profile = React.lazy(() => import("./pages/Profile/Profile"));

const loading = () => <div>Loading...</div>;
const App = () => {
  return (
    <Router>
      <Suspense fallback={loading}>
        <Switch>
          <Route path="/casesum"  component={CaseSum}>
            {/* <About /> */}
          </Route>
          <Route path="/todolist"  component={TodoList}>
            {/* <About /> */}
          </Route>
          <Route path="/todolistredux"  component={TodoListRedux}>
            {/* <About /> */}
          </Route>
          <Route path="/timeline"  component={Timeline}>
            {/* <About /> */}
          </Route>
          <Route path="/profile" component={Profile}></Route>
          <Route path="/" component={Home} />
        </Switch>
      </Suspense>
    </Router>
  );
};
export default App;
