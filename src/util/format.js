import moment from "moment";
import "moment/locale/th";

export const commaFormat = (num, fixed) => {
  if (typeof num != "undefined" && num != null) {
    if (typeof fixed != "undefined" && fixed != null) {
      return num.toFixed(fixed).toLocaleString();
    } else {
      return num.toLocaleString();
    }
  } else {
    return null;
  }
};

export const dateFormat = (date) => {
  moment.locale("th");
  if (typeof date != "undefined" && date != null) {
    return (
      moment(date, "DD/MM/YYYY").format("D MMMM ") + (Number(moment(date, "DD/MM/YYYY").format("Y")) + 543)
    );
  }
};
export const dateFormMMDDYYYY = (date) => {
  moment.locale("th");
  if (typeof date != "undefined" && date != null) {
    return (
      moment(date, "MM/DD/YYYY").format("D MMMM ") + (Number(moment(date, "MM/DD/YYYY").format("Y")) + 543)
    );
  }

}
