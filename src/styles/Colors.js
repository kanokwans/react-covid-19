const Colors = {
    black: "black",
    green: "green",
    green2: "#056738",
    green3: "#179c9b",
    primary: "#e1298e",
    white: "#fff",
    gray: "#666"
};
export default Colors;