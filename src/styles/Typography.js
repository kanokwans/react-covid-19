import Styled from "styled-components";
import Colors from "./Colors";

export const Title = Styled.p`
    color: red; 
    font-weight: bold;
    p {
        color: blue;
    }
`;

export const H1 = Styled.h1`
    font-size: 30px;
    color: ${props => (props.color ? props.color : Colors.black)}
`;

export const H2 = Styled.h2`
    font-size: 20px;
`;

export const A = Styled.a`
    padding: 10px;
    font-size: 25px;
    cursor: pointer;
    text-decoration: none;
    ${({ color }) => (color ? "background-color: " + color + ";" : "")}
    ${({ color }) => (color ? "color: " + Colors.white + ";" : "")}
 `;

//  const { color } = props;
// ${({ color }) => color ? "color: " + Colors.white + ";" : ""}
